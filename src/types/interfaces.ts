export interface QuotesData {
  [key: string]: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    date_added: Date;
    total_supply: 33988299055.005;
    last_updated: Date;
    quote: QuoteData;
  };
}

export interface ConversionData {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: Date;
  quote: QuoteData;
}

export interface CoinMarket {
  data: QuotesData;
}

export interface QuoteData {
  [key: string]: {
    price: number;
    last_updated: Date;
  };
}

export interface InvalidSymbol {
  status: {
    error_message: string | null;
  };
}
export function isInvalid(error: any): error is InvalidSymbol {
  return (error as InvalidSymbol).status.error_message !== null;
}
