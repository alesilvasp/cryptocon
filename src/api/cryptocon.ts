import axios, { AxiosResponse, AxiosInstance } from "axios";
import { InvalidSymbol, CoinMarket } from "../types/interfaces";

export default class CryptoconLib {
  baseURL: string = "https://pro-api.coinmarketcap.com/v1";
  axiosInstance: AxiosInstance;
  api_key: string;

  constructor(api_key: string) {
    this.api_key = api_key;
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
    });
  }

  async quotes(symbol: Array<string>) {
    const requestURL = "/cryptocurrency/quotes/latest";
    let response: AxiosResponse<any, any>;
    try {
      response = await this.axiosInstance.get(requestURL, {
        params: {
          symbol: symbol.join(",").toUpperCase(),
        },
        headers: {
          "X-CMC_PRO_API_KEY": this.api_key,
        },
      });
      return response.data as CoinMarket;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        return error.response?.data as InvalidSymbol;
      }
    }
  }

  async conversion(symbol: string, amount: number, convert: Array<string>) {
    const requestURL = "/tools/price-conversion";
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestURL,
        {
          params: {
            symbol: symbol.toUpperCase(),
            amount: amount,
            convert: convert.join(",").toUpperCase(),
          },
          headers: {
            "X-CMC_PRO_API_KEY": this.api_key,
          },
        }
      );
      return response.data as CoinMarket;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        return error.response?.data as InvalidSymbol;
      }
    }
  }
}
