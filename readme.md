# Cryptocon

Cryptocon é uma lib que faz uma integração com a api [CoinMarketCap](https://coinmarketcap.com/api/) desenvolvida com objetivos acadêmicos, proposta pela Kenzie Academy.

A idéia da lib é buscar na api as informações de cotação e conversão de criptomoedas.

## Instalação

Na raiz do projeto:

`npm i cryptocon`

ou:

`yarn add cryptocon`

## Utilizar

*Para utilizar a lib, é necessário uma api key, que pode ser obtida através do [site oficial](https://coinmarketcap.com/api/) da API.*

faça o import da lib:

```js
import { CryptoconLib } from "cryptocon";
```

Crie uma instância Cryptocon:

```js
const cryptocon = new CryptoconLib(API_KEY)
```

## Métodos

A lib dispõe de dois métodos:

- quotes

É passado um *array* de *string* com os símbolos das cryptomoedas.

```js
cryptocon.quotes(['BTC', 'ADA'])
```

O retorno é uma *promise* que pode ser consultada utilizando o método *then()*

```js
cryptocon.quotes(['BTC', 'ADA']).then((response) => {
  const { data, status } =  response
  console.log(data)
});
```

- conversion

Recebe 3 parâmetros: o *símbolo* da cryptomoeda a ser consultado, o *valor* da conversão e um *array* com os *símbolos a serem convertidos*.

```js
cryptocon.conversion('BTC', 0.005, ['ETC']).then((response) => {
  const { data, status } =  response
  console.log(data)
});
```

## Tecnologias e linguagem utilizadas

- typescript











